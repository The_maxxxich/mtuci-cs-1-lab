total_cost = int (input ("Enter the cost of your dream home: "))
portion_saved = float (input ("Enter the percent of your salary to save, as a decimal: "))
annual_salary = int (input ("Enter yout annual salary: "))
semi_annual_raise = float (input ("Enter the semi-annual raise, as a decimal: "))

current_savings = 0
number_of_months  = 0
r = 0.04

portion_down_payment = total_cost * 0.25
monthly_salary = annual_salary / 12
monthly_saved = monthly_salary * portion_saved

while (current_savings <= portion_down_payment):
    current_savings += current_savings * r / 12 + monthly_saved
    number_of_months += 1

    if number_of_months %6 == 0:
        annual_salary += annual_salary * semi_annual_raise
        monthly_salary = annual_salary / 12
        monthly_saved = monthly_salary * portion_saved

print ("Number of months:" , number_of_months)